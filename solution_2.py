def fib(number):
    if number < 0:
        return "Please enter a positive number!"
    elif number == 1:
        return [0]
    elif number == 2:
        return [0, 1]
    else:
        fibonacci_sequence = [0, 1]
        for i in range(2, number):
            fibonacci_sequence.append(fibonacci_sequence[-2] + fibonacci_sequence[-1])
        return fibonacci_sequence


num = 50
seq = fib(num)
print("sequence", seq)
print("number", seq[-1])
