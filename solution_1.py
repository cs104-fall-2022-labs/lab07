def isPerfect(max_range):
    perfect_list = []
    abundant_list = []
    deficient_list = []

    # for each number
    for number in range(2, max_range + 1):
        factor_list = []
        # check the factors from 1 to that number and collect them
        for factor in range(1, number):
            if number % factor == 0:
                factor_list.append(factor)
        # sum up the factors
        sumFactors = sum(factor_list)

        # classify
        if sumFactors == number:
            perfect_list.append(number)
        elif sumFactors > number:
            # print(number, " is an abundant number")
            abundant_list.append(number)
        else:
            # print(number," is a deficient number")
            deficient_list.append(number)

    print()
    print("SUMMARY")
    print("In the range of 2 to ", max_range, " there are:")
    print(len(perfect_list), " perfect numbers: ", perfect_list)

    print("In the range of 2 to ", max_range, " there are:")
    print(len(abundant_list), " abundant numbers: ", abundant_list)

    print("In the range of 2 to ", max_range, " there are:")
    print(len(deficient_list), " deficient numbers: ", deficient_list)


# get the top of the range
max_range = int(input("Give me the top range to check: "))
isPerfect(max_range)
